# Ansible Role: Docker
Ansible роль устанавливает docker и docker compose на Ubuntu серверы.

Указанные в переменной "docker_users" пользователи, добавляются в группу docker.
Docker compose устанавливается если "install_docker_compose: true"
Compose plugin устанавливается если "install_compose_plugin: true"
Создаёт файл compose.yml (если create_compose_file: true) по пути указанному в переменной "compose_destination". Также по указанному пути будут созданны каталоги.

Для создания compose.yml требуется заполнить переменные по примеру:

    services:
      - name: django-filesharing
        image: "registry.ragnyr.ru/dj${IMAGE_TAG:+:}${IMAGE_TAG}" #если image tag не задан, то image tag = NULL и не ставит :
        ports:
          - 8000:8000
        volumes:
          - /files:/app/public/static/zip
        restart: unless-stopped
        environment:
        networks:
          - filesharing

    networks:
      - name: filesharing
        driver: bridge

Если переменные не указаны, то после рендера шаблона, этих атрибутов в файле не будет. 

compose файл будет создан по пути указанному в переменной:
    compose_destination: ~/filesharing/compose.yml
Также по указанному пути будут созданны каталоги.